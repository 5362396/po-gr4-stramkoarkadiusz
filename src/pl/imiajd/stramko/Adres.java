package pl.imiajd.stramko;

public class Adres {
    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy)
    {
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.kod_pocztowy=kod_pocztowy;
        this.miasto=miasto;
    }

    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy)
    {
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
        this.kod_pocztowy=kod_pocztowy;
        this.miasto=miasto;
    }
    public void pokaz()
    {
        System.out.println(this.kod_pocztowy+" "+this.miasto);
        if(this.numer_mieszkania!=0)
        {
            System.out.println(this.ulica+" "+this.numer_domu+"/"+this.numer_mieszkania);
        }
        else
        {
            System.out.println(this.ulica+" "+this.numer_domu);
        }
    }
    public boolean przed(Adres x)
    {
        int[] tab={0,1,3,4,5};
        StringBuilder dany=new StringBuilder();
        StringBuilder inny=new StringBuilder();
        for(int i : tab)
        {
            dany.append(this.kod_pocztowy.charAt(i));
            inny.append(x.kod_pocztowy.charAt(i));
        }
        int d=Integer.parseInt(dany.toString());
        int i=Integer.parseInt(inny.toString());
        return d<i;
    }

    private String ulica;
    private int numer_domu;
    private int numer_mieszkania=0;
    private String miasto;
    private String kod_pocztowy;
}
