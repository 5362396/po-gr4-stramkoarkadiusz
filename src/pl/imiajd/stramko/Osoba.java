package pl.imiajd.stramko;

public class Osoba {
    public Osoba(String nazwisko, int rokUrodzenia)
    {
        this.nazwisko=nazwisko;
        this.rokUrodzenia=rokUrodzenia;
    }

    public String getNazwisko()
    {
        return this.nazwisko;
    }

    public int getRokUrodzenia()
    {
        return this.rokUrodzenia;
    }

    @Override
    public String toString()
    {
        return "Nazwisko: "+this.nazwisko+"\nRok urodzenia: "+this.rokUrodzenia;
    }

    private String nazwisko;
    private int rokUrodzenia;
}
