package pl.imiajd.stramko;

import java.time.LocalDate;

public abstract class Osoba2
{
    public Osoba2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko=nazwisko;
        this.imiona=imiona;
        this.dataUrodzenia=dataUrodzenia;
        this.plec=plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String[] getImiona()
    {
        return this.imiona;
    }

    public LocalDate getDataUrodzenia()
    {
        return this.dataUrodzenia;
    }

    public String getPlec()
    {
        if (this.plec==true)
        {
            return "Mezczyzna";
        }
        else
        {
            return "Kobieta";
        }
    }

    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;
}
