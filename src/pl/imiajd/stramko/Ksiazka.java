package pl.imiajd.stramko;

public class Ksiazka implements Cloneable, Comparable<Ksiazka> {
    public Ksiazka(String tytul, Autor autor, double cena)
    {
        this.tytul = tytul;
        this.autor = autor;
        this.cena = cena;
    }

    @Override
    public int compareTo(Ksiazka k)
    {
        int comp_nazwa=this.autor.compareTo(k.autor);
        if(comp_nazwa==0)
        {
            int comp_tytul=this.tytul.compareTo(k.tytul);
            if(comp_tytul==0)
                return (int) (this.cena-k.cena);
        }
        return comp_nazwa;
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+"(tytul = "+this.tytul+", autor = "+this.autor.toString()+", cena = "+this.cena+")\n";
    }

    @Override
    public boolean equals(Object o)
    {
        return this.toString().equals(o.toString());
    }

    public Ksiazka clone()
    {
        try
        {
            return (Ksiazka) super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            throw new AssertionError();
        }
    }

    private String tytul;
    private Autor autor;
    private double cena;
}
