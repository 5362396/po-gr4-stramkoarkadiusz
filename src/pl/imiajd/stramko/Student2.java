package pl.imiajd.stramko;

import java.time.LocalDate;

public class Student2 extends Osoba2 {
    public Student2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek=kierunek;
        this.sredniaOcen=sredniaOcen;
    }

    public String getKierunek() {
        return this.kierunek;
    }

    public double getSredniaOcen()
    {
        return this.sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen)
    {
        this.sredniaOcen=sredniaOcen;
    }

    @Override
    public String getOpis()
    {
        return "Kierunek studiow: "+kierunek+"\nSrednia ocen: "+sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}
