package pl.imiajd.stramko;

public class Autor implements Cloneable, Comparable<Autor> {
    public Autor(String nazwa, String email, char plec)
    {
        this.nazwa = nazwa;
        this.email = email;
        this.plec = plec;
    }

    public String getNazwa()
    {
        return this.nazwa;
    }

    public String getEmail()
    {
        return this.email;
    }

    public char getPlec()
    {
        return this.plec;
    }

    public void setNazwa(String nazwa)
    {
        this.nazwa=nazwa;
    }

    public void setEmail(String email)
    {
        this.email=email;
    }

    public void setPlec(char plec)
    {
        this.plec=plec;
    }

    @Override
    public int compareTo(Autor a)
    {
        int comp_nazwa=this.nazwa.compareTo(a.nazwa);
        if(comp_nazwa==0)
        {
            return this.plec-a.plec;
        }
        return comp_nazwa;
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+"[nazwa = "+this.nazwa+", email = "+this.email+", plec = "+this.plec+"]";
    }

    @Override
    public boolean equals(Object o)
    {
        return this.toString().equals(o.toString());
    }

    public Autor clone()
    {
        try
        {
            return (Autor) super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            throw new AssertionError();
        }
    }

    private String nazwa;
    private String email;
    private char plec;
}
