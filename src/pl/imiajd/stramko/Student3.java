package pl.imiajd.stramko;

import java.time.LocalDate;

public class Student3 extends Osoba3 implements Comparable<Osoba3>, Cloneable {
    public Student3(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen)
    {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen=sredniaOcen;
    }

    private double sredniaOcen;

    @Override
    public int compareTo(Osoba3 other)
    {
        int ostatni=super.compareTo((other));
        if((other instanceof Student3) && (ostatni == 0))
        {
            return -(int)Math.ceil(this.sredniaOcen-((Student3) other).sredniaOcen);
        }
        return ostatni;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+" [Nazwisko: "+getNazwisko()+" Data urodzenia: "+getDataUrodzenia()+" Srednia ocen: " + sredniaOcen+"]";
    }
}
