package pl.imiajd.stramko;

import java.time.LocalDate;

public abstract class Instrument {
    public Instrument(String producent, LocalDate rokProdukcji)
    {
        this.producent=producent;
        this.rokProdukcji=rokProdukcji;
    }

    public String getProducent()
    {
        return this.producent;
    }

    public LocalDate getRokProdukcji()
    {
        return this.rokProdukcji;
    }

    public abstract String dzwiek();

    @Override
    public boolean equals(Object obj)
    {
        return this.toString().equals(obj.toString());
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+"\nProducent: "+this.producent+"\nData produkcji: "+this.rokProdukcji+"\n";
    }

    private String producent;
    private LocalDate rokProdukcji;
}
