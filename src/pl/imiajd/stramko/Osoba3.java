package pl.imiajd.stramko;

import java.time.LocalDate;

public class Osoba3 implements Comparable<Osoba3>, Cloneable {

    public Osoba3(String nazwisko, LocalDate dataUrodzenia)
    {
        this.nazwisko=nazwisko;
        this.dataUrodzenia=dataUrodzenia;
    }

    public String getNazwisko()
    {
        return nazwisko;
    }

    public LocalDate getDataUrodzenia()
    {
        return this.dataUrodzenia;
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+" [Nazwisko: "+this.nazwisko+" Data urodzenia: "+this.dataUrodzenia+"]\n";
    }

    @Override
    public boolean equals(Object obj)
    {
        Osoba3 a = (Osoba3) obj;
        return (a.nazwisko.equals(this.nazwisko) && a.dataUrodzenia.equals(this.dataUrodzenia));
    }

    @Override
    public int compareTo(Osoba3 other)
    {
        int porownanie=this.nazwisko.compareTo(other.nazwisko);
        if(porownanie==0)
        {
            return this.dataUrodzenia.compareTo(other.dataUrodzenia);
        }
        return porownanie;
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;
}
