package pl.imiajd.stramko;

import java.time.LocalDate;

public class Pracownik2 extends Osoba2
{
    public Pracownik2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory=pobory;
        this.dataZatrudnienia=dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public LocalDate getDataZatrudnienia()
    {
        return this.dataZatrudnienia;
    }

    @Override
    public String getOpis()
    {
        return String.format("Pracownik zatrudniony w: %s \nPensja: %.2f zł", this.dataZatrudnienia.toString(), this.pobory);
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}
