package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium10;

import java.util.Stack;
import java.util.Scanner;

public class Zadanie5 {
    public static String odwroc_slowa(String zdania)
    {
        Stack<String> stos=new Stack<>();
        String[] slowa=zdania.split(" ");
        StringBuilder odwrocone=new StringBuilder();
        for(String slowo: slowa)
        {
            stos.push(slowo);
            if(slowo.endsWith("."))
            {
                while(!stos.empty())
                {
                    StringBuilder slowo_odwrocone=new StringBuilder();
                    slowo_odwrocone.append(stos.pop());
                    if(stos.empty())
                    {
                        slowo_odwrocone.setCharAt(0,Character.toLowerCase(slowo_odwrocone.charAt(0)));
                        odwrocone.append(slowo_odwrocone);
                        odwrocone.append(". ");
                    }
                    else if(slowo_odwrocone.toString().equals(slowo))
                    {
                        slowo_odwrocone.setCharAt(0,Character.toUpperCase(slowo_odwrocone.charAt(0)));
                        odwrocone.append(slowo_odwrocone, 0, slowo_odwrocone.length()-1);
                        odwrocone.append(" ");
                    }
                    else
                    {
                        odwrocone.append(slowo_odwrocone);
                        odwrocone.append(" ");
                    }
                }
            }
        }
        return odwrocone.toString();
    }
}