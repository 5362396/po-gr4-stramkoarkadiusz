package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium10;

import java.util.LinkedList;

public class Zadanie1 {
    public static <T> void redukuj(LinkedList<T> lista, int n)
    {
        for(int i=0; (i+1)*n-i-1 < lista.size(); i++)
            lista.remove((i+1)*n-i-1);
    }
}
