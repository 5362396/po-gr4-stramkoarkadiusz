package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium10;

import java.util.LinkedList;

public class Zadanie7 {
    public static LinkedList<Integer> sito(int n)
        {
        LinkedList<Integer> pierwsze=new LinkedList<Integer>();
        for(int i=1; i<n+1; i++)
            pierwsze.add(i);
        for(int i=2; i<=n/2; i++)
            for(int j=2; j*i<=n; j++)
                pierwsze.remove((Integer)(i*j));
        return pierwsze;
    }
}

