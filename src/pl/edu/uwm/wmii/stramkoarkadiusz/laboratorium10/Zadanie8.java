package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium10;

import java.util.Iterator;

public class Zadanie8 {
    public static <T extends Iterable<?>> void print(T object )
    {
        Iterator<?> it= object.iterator();
        while (it.hasNext()){
            System.out.print(it.next());
            if (it.hasNext()){
                System.out.print(", ");
            }
        }
        System.out.println();
    }
}
