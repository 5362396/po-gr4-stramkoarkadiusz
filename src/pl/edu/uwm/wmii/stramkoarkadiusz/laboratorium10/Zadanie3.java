package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium10;

import java.util.LinkedList;

public class    Zadanie3 {
    public static <T> void odwroc(LinkedList<T> lista)
    {
        T zm;
        for(int i=0; i<lista.size()/2; i++)
        {
            zm = lista.get(i);
            lista.set(i, lista.get(lista.size()-i-1));
            lista.set(lista.size()-i-1, zm);
        }
    }
}
