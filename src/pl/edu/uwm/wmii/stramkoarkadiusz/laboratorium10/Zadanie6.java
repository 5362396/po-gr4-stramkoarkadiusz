package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium10;

import java.util.Stack;

public class Zadanie6 {
    public static void podziel(int liczba) {
        Stack<Integer> stos=new Stack<Integer>();
        while(liczba>0)
        {
            stos.add(liczba%10);
            liczba = liczba/10;
        }
        while(!stos.empty())
        {
            System.out.print(stos.pop()+" ");
        }
        System.out.println();
    }
}

