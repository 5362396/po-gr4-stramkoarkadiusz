package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium06;

import pl.imiajd.stramko.*;

public class TestAdres {
    public static void main(String[] args) {
        Adres a = new Adres("Polna", 10, "Olsztyn", "11-345");
        Adres b = new Adres("Rolna", 6, 2, "Olsztyn", "11-348");
        a.pokaz();
        b.pokaz();
        System.out.println(a.przed(b));
        System.out.println(b.przed(a));
    }
}
