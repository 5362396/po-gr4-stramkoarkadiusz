package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium06;

import pl.imiajd.stramko.*;

public class TestOsobaStudentNauczyciel {
    public static void main(String[] args) {
        Osoba a=new Osoba("Kowalski", 1984);
        Student b=new Student("Nowak", 2000, "Informatyka");
        Nauczyciel c=new Nauczyciel("Polak", 1979, 3000);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(a.getNazwisko());
        System.out.println(b.getKierunek());
        System.out.println(c.getPensja());
    }
}
