package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie3 {
    public static int wyrazy(String sciezka, String wyraz)
    {
        try
        {
            int ilosc=0;
            File plik=new File(sciezka);
            String linijka;
            Scanner scnr=new Scanner(plik);
            while(scnr.hasNextLine())
            {
                linijka=scnr.nextLine();
                ilosc=ilosc+Zadanie1.countSubStr(linijka, wyraz);
            }
            return ilosc;
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Zla sciezka pliku");
            e.printStackTrace();
            return 0;
        }

    }
}
