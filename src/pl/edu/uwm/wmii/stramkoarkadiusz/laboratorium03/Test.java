package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium03;

import java.util.Arrays;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scnr=new Scanner(System.in);
        System.out.println("Zadanie 1a:");
        String str_a="aaba ca";
        char c='a';
        System.out.println("Ilosc wystapien znaku w napisie: "+Zadanie1.countChar(str_a, c));
        System.out.println("Zadanie 1b:");
        String str_b="jdjd jdij d";
        String str_b2="jd";
        System.out.println("Ilosc wystapien drugiego napisu w pierwszym napisie: "+Zadanie1.countSubStr(str_b, str_b2));
        System.out.println("Zadanie 1c:");
        System.out.println("Wprowadz napis");
        String str_c=scnr.nextLine();
        System.out.println("Srodek podanego napisu: "+Zadanie1.middle(str_c));
        System.out.println("Zadanie 1d:");
        System.out.println("Wprowadz napis");
        String str_d=scnr.nextLine();
        System.out.println("Wprowadz ile razy chcesz skopiowac napis");
        int n=scnr.nextInt();
        System.out.println("Napis po zmianie: "+Zadanie1.repeat(str_d, n));
        System.out.println("Zadanie 1e:");
        System.out.println("Zawartosc tablicy: "+ Arrays.toString(Zadanie1.where(str_b, str_b2)));
        System.out.println("Zadanie 1f:");
        String str_f="fPdeRb";
        System.out.println("Napis po zmianach: "+ Zadanie1.change(str_f));
        System.out.println("Zadanie 1g:");
        String str_g="24637325352678327806456";
        System.out.println("Napis po zmianach: "+ Zadanie1.nice(str_g));
        System.out.println("Zadanie 1h:");
        System.out.println("Wprowadz separator");
        char separator=scnr.next().charAt(0);
        System.out.println("Wprowadz liczbe pozycji pomiedzy kolejnymi wystapieniami separatora");
        int pozycje=scnr.nextInt();
        System.out.println("Napis po zmianach: "+ Zadanie1.nice(str_g, separator, pozycje));
        System.out.println("Zadanie 2:");
        System.out.println(Zadanie2.znaki("C:\\Users\\Arek\\IdeaProjects\\po-gr4-stramkoarkadiusz\\src\\pl\\edu\\uwm\\wmii\\stramkoarkadiusz\\laboratorium03\\znak.txt", 'a'));
        System.out.println("Zadanie 3:");
        System.out.println(Zadanie3.wyrazy("C:\\Users\\Arek\\IdeaProjects\\po-gr4-stramkoarkadiusz\\src\\pl\\edu\\uwm\\wmii\\stramkoarkadiusz\\laboratorium03\\wyraz.txt", "co"));
        System.out.println("Zadanie 4:");
        Zadanie4.szachownica(3);
        System.out.println("Zadanie 5:");
        System.out.println(Zadanie5.kapital(10, 0.05, 20));
    }
}
