package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium03;

import java.util.Arrays;
import java.lang.StringBuffer;

public class Zadanie1 {
    public static int countChar(String str, char c)
    {
        int ile=0;
        for(int i=0; i<str.length(); i++)
        {
            if(str.charAt(i)==c)
            {
                ile++;
            }
        }
        return ile;
    }
    public static int countSubStr(String str, String subStr)
    {
        int ile=0;
        int n=0;
        for(int i=0; i<str.length(); i++)
        {
            if(str.charAt(i)==subStr.charAt(0))
            {
                for(int j=0; j<subStr.length(); j++)
                {
                    if(str.charAt(i+j)==subStr.charAt(j))
                    {
                        n++;
                    }
                }
                if(subStr.length()==n)
                {
                    ile++;
                }
                n=0;
            }
        }
        return ile;
    }
    public static String middle(String str)
    {
        String srodek="";
        if(str.length()<=1)
        {
            return "Za krotki napis, brak srodka";
        }
        if(str.length()%2==0)
        {
            srodek+=str.charAt(str.length()/2-1);
            srodek+=str.charAt(str.length()/2);
        }
        else
        {
            srodek+=str.charAt(str.length()/2);
        }
        return srodek;
    }
    public static String repeat(String str, int n)
    {
        String kopia="";
        for(int i=0; i<n; i++)
        {
            kopia+=str;
        }
        return kopia;
    }
    public static int[] where(String str, String subStr)
    {
        int[] tab=new int[0];
        int fromIndex=0;
        while((fromIndex=str.indexOf(subStr, fromIndex))!=-1)
        {
            tab=Arrays.copyOf(tab,tab.length+1);
            tab[tab.length-1]=fromIndex;
            fromIndex++;
        }
        return tab;
    }
    public static String change(String str)
    {
        char[] tab=new char[str.length()];
        StringBuffer sb=new StringBuffer(str.length());
        for(int i=0; i<str.length(); i++)
        {
            tab[i]=str.charAt(i);
        }
        char[] tab2=new char[str.length()];
        for(int k=0; k<str.length(); k++)
        {
            if(Character.isUpperCase(tab[k]))
            {
                tab2[k]=Character.toLowerCase(tab[k]);
            }
            else
            {
                tab2[k]=Character.toUpperCase(tab[k]);
            }
        }
        for(char c : tab2)
        {
            sb.append(c);
        }
        return sb.toString();
    }
    public static String nice(String str)
    {
        StringBuffer napis=new StringBuffer();
        char litera;
        char separator='\'';
        int n=0;
        int pocz=str.length()%3;
        for(int i=0; i<pocz; i++)
        {
            litera=str.charAt(i);
            napis.append(litera);
            if(i==pocz-1)
            {
                napis.append(separator);
            }
        }
        for(int j=pocz; j<str.length(); j++)
        {
            litera=str.charAt(j);
            napis.append(litera);
            n=n+1;
            if(n%3==0 && j!=str.length()-1)
            {
                napis.append(separator);
            }
        }
        return napis.toString();
    }
    public static String nice(String str, char separator, int pozycje)
    {
        StringBuffer napis=new StringBuffer();
        char litera;
        int n=0;
        int pocz=str.length()%pozycje;
        for(int i=0; i<pocz; i++)
        {
            litera=str.charAt(i);
            napis.append(litera);
            if(i==pocz-1)
            {
                napis.append(separator);
            }
        }
        for(int j=pocz; j<str.length(); j++)
        {
            litera=str.charAt(j);
            napis.append(litera);
            n=n+1;
            if(n%pozycje==0 && j!=str.length()-1)
            {
                napis.append(separator);
            }
        }
        return napis.toString();
    }
}
