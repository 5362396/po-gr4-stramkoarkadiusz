package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium09;
import pl.imiajd.stramko.Osoba3;

import java.time.LocalDate;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Integer[] posortowane=new Integer[]{1,2,3,5,6};
        Integer[] nieposortowane=new Integer[]{2,3,6,5,1};
        LocalDate[] posortowane2=new LocalDate[]{LocalDate.of(1967, 4, 22), LocalDate.of(1989, 12, 16), LocalDate.of(2000, 5, 11), LocalDate.of(2006, 8, 3)};
        LocalDate[] nieposortowane2=new LocalDate[]{LocalDate.of(2000, 5, 11), LocalDate.of(2006, 8, 3), LocalDate.of(1967, 4, 22), LocalDate.of(1989, 12, 16)};
        System.out.println(ArrayUtil.isSorted(posortowane));
        System.out.println(ArrayUtil.isSorted(nieposortowane));
        System.out.println(ArrayUtil.isSorted(posortowane2));
        System.out.println(ArrayUtil.isSorted(nieposortowane2));
        System.out.print("\n");
        System.out.println(ArrayUtil.binSearch(nieposortowane,3));
        System.out.println(ArrayUtil.binSearch(nieposortowane2, LocalDate.of(1989, 12, 16)));
        System.out.print("\n");
        ArrayUtil.selectionSort(nieposortowane);
        ArrayUtil.selectionSort(nieposortowane2);
        for (int i : nieposortowane)
        {
            System.out.println(i);
        }
        for (LocalDate i : nieposortowane2)
        {
            System.out.println(i);
        }
        System.out.print("\n");
        ArrayList<Integer> nieposortowane3=new ArrayList<>();
        nieposortowane3.add(2);
        nieposortowane3.add(3);
        nieposortowane3.add(6);
        nieposortowane3.add(5);
        nieposortowane3.add(1);
        ArrayList<LocalDate> nieposortowane4=new ArrayList<>();
        nieposortowane4.add(LocalDate.of(2000, 5, 11));
        nieposortowane4.add(LocalDate.of(2006, 8, 3));
        nieposortowane4.add(LocalDate.of(1967, 4, 22));
        nieposortowane4.add(LocalDate.of(1989, 12, 16));
        ArrayUtil.mergeSort(nieposortowane3);
        ArrayUtil.mergeSort(nieposortowane4);
        for(int i=0; i<5; i++)
        {
            System.out.println(nieposortowane3.get(i).toString());
        }
        for(int i=0; i<4; i++)
        {
            System.out.println(nieposortowane4.get(i).toString());
        }
    }
}
