package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium09;
import java.util.ArrayList;

public class ArrayUtil<T> {
    T[] tab;

    public ArrayUtil(T[] tab)
    {
        this.tab=tab;
    }

    static <T extends Comparable> boolean isSorted(T[] t)
    {
        if(t == null || t.length == 0)
        {
            return false;
        }
        for(int i=1; i<t.length; i++)
        {
            if(t[i-1].compareTo(t[i])>0)
            {
                return false;
            }
        }
        return true;
    }

    static <T extends Comparable> int binSearch(T[] t, T e)
    {
        int index=-1;
        for(int i=0; i<t.length; i++)
        {
            if(t[i].equals(e))
            {
                index=i;
                break;
            }
        }
        return index;

    }

    static <T extends Comparable> void selectionSort(T[] t)
    {
        T temp;
        for(int i=0; i<t.length; i++)
        {
            for(int j=i; j<t.length; j++)
            {
                if(t[i].compareTo(t[j])>=0)
                {
                    temp = t[j];
                    t[j] = t[i];
                    t[i] = temp;
                }
            }
        }
    }

    public static <T extends Comparable<? super T>> void mergeSort(ArrayList<T> t)
    {
        if(t.size()>1)
        {
            ArrayList<T> lewe=new ArrayList<>();
            ArrayList<T> prawe=new ArrayList<>();
            boolean przelacznik=true;
            while(!t.isEmpty())
            {
                if(przelacznik)
                {
                    lewe.add(t.remove(0));
                }
                else
                {
                    prawe.add(t.remove(t.size()/2));
                }
                przelacznik = !przelacznik;
            }
            mergeSort(lewe);
            mergeSort(prawe);
            while(!lewe.isEmpty() && !prawe.isEmpty())
            {
                if(lewe.get(0).compareTo(prawe.get(0))<=0)
                {
                    t.add(lewe.remove(0));
                }
                else
                {
                    t.add(prawe.remove(0));
                }
            }
            if(!lewe.isEmpty())
            {
                t.addAll(lewe);
            }
            else if(!prawe.isEmpty())
            {
                t.addAll(prawe);
            }
        }
    }
}