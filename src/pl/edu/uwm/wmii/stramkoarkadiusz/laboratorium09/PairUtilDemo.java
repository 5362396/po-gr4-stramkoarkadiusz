package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium09;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class PairUtilDemo {
    public static void main(String[] args)
    {
        GregorianCalendar[] birthdays = {
                new GregorianCalendar (1906, Calendar.DECEMBER, 9), // G. Hopper
                new GregorianCalendar (1815, Calendar.DECEMBER, 10), // A. Lovelace
                new GregorianCalendar (1903, Calendar.DECEMBER, 3), // J. von Neumann
                new GregorianCalendar (1910, Calendar.JUNE, 22), // K. Zuse
        };
        Pair<GregorianCalendar> mm = ArrayAlg.minmax (birthdays);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        System.out.println ("min = " + sdf.format(mm.getFirst().getTime()));
        System.out.println ("max = " + sdf.format(mm.getSecond().getTime()));
        //test zadanie 1
        int a=4;
        int b=3;
        String[] transport={"Rower", "Autobus", "Pociag", "Samochod"};
        PairUtil<String> para = ArrayAlgUtil.minmax(transport);
        System.out.println("min = " + para.getFirst());
        System.out.println("max = " + para.getSecond());
        PairUtil.swap(para);
        System.out.println("min = " + para.getFirst());
        System.out.println("max = " + para.getSecond());
    }
}

class ArrayAlgUtil {
    public static <T extends Comparable<? super T>> PairUtil<T> minmax (T[] a) {
        if (a == null || a.length == 0) return null;
        T min = a[0];
        T max = a[0];
        for (int i = 1; i < a.length; i++) {
            if (min.compareTo (a[i]) > 0) min = a[i];
            if (max.compareTo (a[i]) < 0) max = a[i];
        }
        return new PairUtil<T> (min, max);
    }
}