package pl.edu.uwm.wmii.stramkoarkadiusz.test1;

import java.util.Scanner;

public class Zadanie1 {
    public static void ile(int n) {
        Scanner scnr=new Scanner(System.in);
        double[] rzeczywiste=new double[n];
        int wieksze=0;
        int mniejsze=0;
        int rowne=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Wczytaj "+(i+1)+" liczbe rzeczywista");
            rzeczywiste[i]=scnr.nextDouble();
        }
        for(int j=0; j<n; j++)
        {
            if(rzeczywiste[j]>5)
            {
                wieksze++;
            }
            else if(rzeczywiste[j]<-5)
            {
                mniejsze++;
            }
            else if(rzeczywiste[j]==-5)
            {
                rowne++;
            }
        }
        System.out.println("Wpisano "+wieksze+" liczb wiekszych od 5");
        System.out.println("Wpisano "+mniejsze+" liczb mniejszych od -5");
        System.out.println("Wpisano "+rowne+" liczb rownych -5");
    }
}
