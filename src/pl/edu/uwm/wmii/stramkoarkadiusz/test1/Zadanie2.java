package pl.edu.uwm.wmii.stramkoarkadiusz.test1;

public class Zadanie2 {
    public static String delete(String str, char c)
    {
        StringBuilder mod=new StringBuilder();
        for(int i=0; i<str.length(); i++)
        {
            if(str.charAt(i)=='a' || str.charAt(i)=='A' || str.charAt(i)=='e' || str.charAt(i)=='E' || str.charAt(i)=='y' || str.charAt(i)=='Y' || str.charAt(i)=='u' || str.charAt(i)=='U' || str.charAt(i)=='i' || str.charAt(i)=='I' || str.charAt(i)=='o' || str.charAt(i)=='O')
            {
                mod.append(c);
            }
            else
            {
                mod.append(str.charAt(i));
            }
        }
        return mod.toString();
    }
}
