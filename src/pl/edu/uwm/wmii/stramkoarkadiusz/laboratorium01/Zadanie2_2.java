package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium01;

import java.util.Scanner;

public class Zadanie2_2 {
    public static double a(int n)
    {
        Scanner scnr=new Scanner(System.in);
        double suma=0;
        for (int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            double wpisana=scnr.nextDouble();
            if (wpisana>0) {
                suma+=wpisana;
            }
        }
        return 2*suma;
    }
}