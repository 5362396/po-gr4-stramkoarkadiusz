package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium01;

import java.util.Scanner;

public class Zadanie2_4 {
    public static void a(int n)
    {
        Scanner scnr=new Scanner(System.in);
        System.out.println("Podaj liczbę numer 1");
        double wpisana=scnr.nextDouble();
        double min=wpisana;
        double max=wpisana;
        for(int i=1; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            wpisana=scnr.nextDouble();
            if(wpisana>max)
            {
                max=wpisana;
            }
            if(wpisana<min)
            {
                min=wpisana;
            }
        }
        System.out.println("Najmniejsza z liczb to "+min);
        System.out.println("Najwieksza z liczb to "+max);
    }
}
