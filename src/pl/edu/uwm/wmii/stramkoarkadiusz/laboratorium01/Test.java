package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium01;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scnr=new Scanner(System.in);
        int n;
        System.out.println("Podaj po ile liczb chcesz wczytywać do każdego podpunktu w zadaniach");
        n=scnr.nextInt();
        System.out.println("Zadanie 1_1a:");
        System.out.println(Zadanie1_1.a(n));
        System.out.println("Zadanie 1_1b:");
        System.out.println(Zadanie1_1.b(n));
        System.out.println("Zadanie 1_1c:");
        System.out.println(Zadanie1_1.c(n));
        System.out.println("Zadanie 1_1d:");
        System.out.println(Zadanie1_1.d(n));
        System.out.println("Zadanie 1_1e:");
        System.out.println(Zadanie1_1.e(n));
        System.out.println("Zadanie 1_1f:");
        System.out.println(Zadanie1_1.f(n));
        System.out.println("Zadanie 1_1g:");
        Zadanie1_1.g(n);
        System.out.println("zadanie 1_1h:");
        System.out.println(Zadanie1_1.h(n));
        System.out.println("zadanie 1_1i:");
        System.out.println(Zadanie1_1.i(n));
        System.out.println("zadanie 1_2:");
        Zadanie1_2.a(n);
        System.out.println("Zadanie 2_1a:");
        System.out.println(Zadanie2_1.a(n));
        System.out.println("Zadanie 2_1b:");
        System.out.println(Zadanie2_1.b(n));
        System.out.println("Zadanie 2_1c:");
        System.out.println(Zadanie2_1.c(n));
        System.out.println("Zadanie 2_1d:");
        System.out.println(Zadanie2_1.d(n));
        System.out.println("Zadanie 2_1e:");
        System.out.println(Zadanie2_1.e(n));
        System.out.println("Zadanie 2_1f:");
        System.out.println(Zadanie2_1.f(n));
        System.out.println("Zadanie 2_1g:");
        System.out.println(Zadanie2_1.g(n));
        System.out.println("Zadanie 2_1h:");
        System.out.println(Zadanie2_1.h(n));
        System.out.println("Zadanie 2_2:");
        System.out.println(Zadanie2_2.a(n));
        System.out.println("Zadanie 2_3:");
        Zadanie2_3.a(n);
        System.out.println("Zadanie 2_4:");
        Zadanie2_4.a(n);
        System.out.println("Zadanie 2_5:");
        System.out.println(Zadanie2_5.a(n));
    }
}
