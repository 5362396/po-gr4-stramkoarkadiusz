package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium01;

import java.util.Scanner;

public class Zadanie2_3 {
    public static void a(int n)
    {
        Scanner scnr = new Scanner(System.in);
        int dodatnie=0;
        int ujemne=0;
        int zera=0;
        for(int i=0;i<n;i++) {
            System.out.println("Podaj liczbę numer "+(i+1));
            double wpisana=scnr.nextDouble();
            if(wpisana>0)
            {
                dodatnie+=1;
            }
            else if(wpisana<0)
            {
                ujemne+=1;
            }
            else
            {
                zera+=1;
            }
        }
        System.out.println("Ilosc liczb dodatnich "+dodatnie);
        System.out.println("Ilosc liczb ujemnych "+ujemne);
        System.out.println("Ilosc zer "+zera);
    }
}
