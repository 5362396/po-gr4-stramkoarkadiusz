package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium01;

import java.util.Scanner;

public class Zadanie2_1 {
    public static int a(int n)
    {
        Scanner scnr=new Scanner(System.in);
        int wpisana[]=new int[n];
        int ile=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            wpisana[i]=scnr.nextInt();
            if(wpisana[i]%2==1)
            {
                ile++;
            }
        }
        return ile;
    }
    public static int b(int n)
    {
        Scanner scnr=new Scanner(System.in);
        int wpisana[]=new int[n];
        int ile=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            wpisana[i]=scnr.nextInt();
            if(wpisana[i]%3==0 && wpisana[i]%5!=0)
            {
                ile++;
            }
        }
        return ile;
    }
    public static int c(int n)
    {
        Scanner scnr=new Scanner(System.in);
        int wpisana[]=new int[n];
        int ile=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            wpisana[i]=scnr.nextInt();
            if(Math.sqrt(wpisana[i])%2==0)
            {
                ile++;
            }
        }
        return ile;
    }
    public static int d(int n)
    {
        Scanner scnr=new Scanner(System.in);
        int wpisana[]=new int[n];
        int ile=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            wpisana[i]=scnr.nextInt();
        }
        for(int i=1; i<n-1; i++)
        {
            if(wpisana[i]<(wpisana[i-1]+wpisana[i+1])/2)
            {
                ile++;
            }
        }
        return ile;
    }
    public static int e(int n)
    {
        Scanner scnr=new Scanner(System.in);
        int wpisana[]=new int[n];
        int ile=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            wpisana[i]=scnr.nextInt();
        }
        for(int i=0; i<n; i++)
        {
            if(Math.pow(2,i+1)<wpisana[i] && wpisana[i]<Zadanie1_1.silnia(i+1))
            {
                ile++;
            }
        }
        return ile;
    }
    public static int f(int n)
    {
        Scanner scnr=new Scanner(System.in);
        int wpisana[]=new int[n+1];
        int ile=0;
        for(int i=1; i<n+1; i++)
        {
            System.out.println("Podaj liczbę numer "+(i));
            wpisana[i]=scnr.nextInt();
        }
        for(int i=1; i<n+1; i=i+2)
        {
            if(wpisana[i]%2==0)
            {
                ile++;
            }
        }
        return ile;
    }
    public static int g(int n)
    {
        Scanner scnr=new Scanner(System.in);
        int wpisana[]=new int[n];
        int ile=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            wpisana[i]=scnr.nextInt();
            if(wpisana[i]%2==1 && wpisana[i]>=0)
            {
                ile++;
            }
        }
        return ile;
    }
    public static int h(int n)
    {
        Scanner scnr=new Scanner(System.in);
        int wpisana[]=new int[n];
        int ile=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            wpisana[i]=scnr.nextInt();
        }
        for(int i=0; i<n; i++)
        {
            if(Math.abs(wpisana[i])<Math.pow(i+1,2))
            {
                ile++;
            }
        }
        return ile;
    }
}
