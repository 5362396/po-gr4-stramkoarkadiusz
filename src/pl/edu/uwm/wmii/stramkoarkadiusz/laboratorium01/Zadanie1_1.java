package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium01;

import java.util.Scanner;

public class Zadanie1_1 {
    public static double a(int n)
    {
        Scanner scnr=new Scanner(System.in);
        double suma=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            double wpisana=scnr.nextDouble();
            suma+=wpisana;
        }
        return suma;
    }
    public static double b(int n)
    {
        Scanner scnr=new Scanner(System.in);
        double iloczyn=1;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            double wpisana=scnr.nextDouble();
            iloczyn*=wpisana;
        }
        return iloczyn;
    }
    public static double c(int n)
    {
        Scanner scnr=new Scanner(System.in);
        double suma=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            double wpisana=scnr.nextDouble();
            suma+=Math.abs(wpisana);
        }
        return suma;
    }
    public static double d(int n)
    {
        Scanner scnr=new Scanner(System.in);
        double suma=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            double wpisana=scnr.nextDouble();
            suma+=Math.sqrt(Math.abs(wpisana));
        }
        return suma;
    }
    public static double e(int n)
    {
        Scanner scnr=new Scanner(System.in);
        double iloczyn=1;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            double wpisana=scnr.nextDouble();
            iloczyn*=Math.abs(wpisana);
        }
        return iloczyn;
    }
    public static double f(int n)
    {
        Scanner scnr=new Scanner(System.in);
        double suma=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            double wpisana=scnr.nextDouble();
            suma+=Math.pow(wpisana,2);
        }
        return suma;
    }
    public static void g(int n)
    {
        Scanner scnr=new Scanner(System.in);
        double suma=0;
        double iloczyn=1;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            double wpisana=scnr.nextDouble();
            suma+=wpisana;
            iloczyn*=wpisana;
        }
        System.out.println("Suma: "+suma);
        System.out.println("Iloczyn: "+iloczyn);
    }
    public static double h(int n)
    {
        Scanner scnr=new Scanner(System.in);
        double suma=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            double wpisana=scnr.nextDouble();
            suma+=Math.pow(-1,i)*wpisana;
        }
        return suma;
    }
    public static double i(int n)
    {
        Scanner scnr=new Scanner(System.in);
        double suma=0;
        for(int i=0; i<n; i++)
        {
            System.out.println("Podaj liczbę numer "+(i+1));
            double wpisana=scnr.nextDouble();
            suma+=Math.pow(-1,i+1)*(wpisana/silnia(i+1));
        }
        return suma;
    }
    public static int silnia(int n)
    {
        if(n==1)
        {
            return 1;
        }
        else
        {
            return silnia(n-1)*n;
        }
    }
}
