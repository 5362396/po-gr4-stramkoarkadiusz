package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium01;

import java.util.Scanner;

public class Zadanie2_5 {
    public static int a(int n)
    {
        Scanner scnr=new Scanner(System.in);
        int ilosc=0;
        double wpisana[]=new double[n];
        for(int i=0; i<n; i++)
        {
            wpisana[i]=scnr.nextDouble();
        }
        for(int i=0; i<n-1; i++)
        {
            if(wpisana[i]>0 && wpisana[i+1]>0)
            {
                ilosc++;
            }
        }
        return ilosc;
    }
}
