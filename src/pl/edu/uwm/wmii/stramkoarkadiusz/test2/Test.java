package pl.edu.uwm.wmii.stramkoarkadiusz.test2;

import pl.imiajd.stramko.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class Test {
    public static void main(String[] args) {
        ArrayList<Autor> lista_autorow = new ArrayList<>();
        lista_autorow.add(0, new Autor("Brzechwa","jan.brzechwa@gmail.com",'m'));
        lista_autorow.add(1, new Autor("Szymborska","wieslawa.szymborska@o2.pl",'k'));
        lista_autorow.add(2, new Autor("Tuwim","julian.tuwim@wp.pl",'m'));
        lista_autorow.add(3, new Autor("Szymborska","ws@onet.pl",'k'));
        System.out.println("Przed Sortowaniem:");
        for (Autor element : lista_autorow)
        {
            System.out.print(element.toString());
            System.out.println("");
        }
        System.out.println("\n");
        Collections.sort(lista_autorow);
        System.out.println("Po Sortowaniu:");
        for (Autor autor : lista_autorow)
        {
            System.out.print(autor.toString());
            System.out.println("");
        }
        System.out.println("\n");
        ArrayList<Ksiazka> lista_ksiazek = new ArrayList<>();
        lista_ksiazek.add(0, new Ksiazka("101 wierszy", lista_autorow.get(2),75.99));
        lista_ksiazek.add(1, new Ksiazka("Akademia pana Kleksa", lista_autorow.get(0),49.99));
        lista_ksiazek.add(2, new Ksiazka("Milczenie roslin", lista_autorow.get(2),56.99));
        lista_ksiazek.add(3, new Ksiazka("Lokomotywa", lista_autorow.get(3),82.99));
        System.out.println("Przed Sortowaniem:");
        for (Ksiazka item : lista_ksiazek)
        {
            System.out.print(item.toString());
        }
        Collections.sort(lista_ksiazek);
        System.out.println("\n");
        System.out.println("Po Sortowaniu:");
        for (Ksiazka value : lista_ksiazek)
        {
            System.out.print(value.toString());
        }
        System.out.println("\nZadanie 2");
        System.out.println("");
        LinkedList<Ksiazka> ksiazki = new LinkedList<>();
        ksiazki.add(0, new Ksiazka("101 wierszy", lista_autorow.get(2),75.99));
        ksiazki.add(1, new Ksiazka("Akademia pana Kleksa", lista_autorow.get(0),49.99));
        ksiazki.add(2, new Ksiazka("Milczenie roslin", lista_autorow.get(2),56.99));
        ksiazki.add(3, new Ksiazka("Lokomotywa", lista_autorow.get(3),82.99));
        System.out.println("Przed usuwaniem");
        for (Ksiazka ksiazka : ksiazki)
        {
            System.out.print(ksiazka.toString());
        }
        System.out.println("\n");
        System.out.println("Po usuwaniu");
        Redukuj.redukuj(ksiazki, 2);
    }
}
