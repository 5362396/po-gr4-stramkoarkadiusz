package pl.edu.uwm.wmii.stramkoarkadiusz.test2;

import java.util.LinkedList;

public class Redukuj {
    public static <T> void redukuj(LinkedList<T> books, int n)
    {
        for (int i=n-1; i<books.size(); i+=(n-1))
        {
            books.remove(i);
        }
        for (T book : books)
        {
            System.out.print(book.toString());
        }
    }
}
