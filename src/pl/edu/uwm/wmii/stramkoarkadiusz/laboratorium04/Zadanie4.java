package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium04;

import java.util.ArrayList;

public class Zadanie4 {
    public static ArrayList<Integer> reversed(ArrayList<Integer> a)
    {
        ArrayList<Integer> koncowa=new ArrayList<>();
        for(int i=0; i<a.size(); i++)
        {
            koncowa.add(a.get(a.size()-1-i));
        }
        return koncowa;
    }
}
