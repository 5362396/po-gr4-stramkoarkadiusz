package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium04;

import java.util.ArrayList;

public class Zadanie2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> koncowa=new ArrayList<>();
        if(a.size()<b.size())
        {
            for(int i=0; i<a.size(); i++)
            {
                koncowa.add(a.get(i));
                koncowa.add(b.get(i));
            }
            for(int i=a.size(); i<b.size(); i++)
            {
                koncowa.add(b.get(i));
            }
        }
        else
        {
            for(int i=0; i<b.size(); i++)
            {
                koncowa.add(a.get(i));
                koncowa.add(b.get(i));
            }
            for(int i=b.size(); i<a.size(); i++)
            {
                koncowa.add(a.get(i));
            }
        }

        return koncowa;
    }
}
