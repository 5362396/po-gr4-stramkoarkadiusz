package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium04;

import java.util.ArrayList;

public class Zadanie5 {
    public static void reverse(ArrayList<Integer> a)
    {
        int tmp;
        for(int i=0; i<a.size()/2; i++)
        {
            tmp=a.get(i);
            a.set(i, a.get(a.size()-1-i));
            a.set(a.size()-1-i, tmp);
        }
    }
}
