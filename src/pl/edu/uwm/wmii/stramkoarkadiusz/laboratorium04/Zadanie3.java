package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium04;

import java.util.ArrayList;

public class Zadanie3 {
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> koncowa=Zadanie1.append(a,b);
        int size=a.size()+b.size();

        for(int i=0; i<size-1; i++)
        {
            for(int j=0; j<size-i-1; j++)
            {
                if(koncowa.get(j)>koncowa.get(j+1))
                {
                    int tmp=koncowa.get(j);
                    koncowa.set(j,koncowa.get(j+1));
                    koncowa.set(j+1,tmp);
                }
            }
        }
        return koncowa;
    }
}
