package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium04;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        ArrayList<Integer> a=new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b=new ArrayList<>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        System.out.println(a);
        System.out.println(b);
        System.out.println("Zadanie 1:");
        System.out.println(Zadanie1.append(a,b));
        System.out.println("Zadanie 2:");
        System.out.println(Zadanie2.merge(a,b));
        System.out.println("Zadanie 3:");
        System.out.println(Zadanie3.mergeSorted(a,b));
        System.out.println("Zadanie 4:");
        System.out.println(Zadanie4.reversed(a));
        System.out.println("Zadanie 5:");
        Zadanie5.reverse(b);
        System.out.println(b);
    }
}
