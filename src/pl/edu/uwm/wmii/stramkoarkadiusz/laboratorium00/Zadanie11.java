package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium00;

public class Zadanie11
{
    public static void main(String args[])
    {
        System.out.println("Żeby kózka nie skakała,");
        System.out.println("Toby nóżki nie złamała.");
        System.out.println("Ale gdyby nie skakała,");
        System.out.println("Toby smutne życie miała.");
    }
}