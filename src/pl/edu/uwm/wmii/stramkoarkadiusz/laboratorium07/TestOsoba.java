package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium07;

import pl.imiajd.stramko.*;
import java.time.LocalDate;
import java.util.Arrays;

public class TestOsoba {
    public static void main(String[] args) {
        Pracownik2 a=new Pracownik2("Kowalski", new String[]{"Jan", "Mariusz"}, LocalDate.of(1976, 6,21), true, 300.50, LocalDate.of(2008, 7, 13));
        Student2 b=new Student2("Nowakowska", new String[]{"Anna"}, LocalDate.of(1989, 12,16), false, "Informatyka", 4.6);
        System.out.println(a.getNazwisko());
        for(String i : a.getImiona())
        {
            System.out.print(i+"\n");
        }
        System.out.println(a.getDataUrodzenia());
        System.out.println(a.getPlec());
        System.out.println(a.getPobory());
        System.out.println(a.getDataZatrudnienia());
        System.out.println(a.getOpis());
        for(String i : b.getImiona())
        {
            System.out.print(i+"\n");
        }
        System.out.println(b.getKierunek());
        System.out.println(b.getSredniaOcen());
        b.setSredniaOcen(4.7);
        System.out.println(b.getOpis());
    }
}
