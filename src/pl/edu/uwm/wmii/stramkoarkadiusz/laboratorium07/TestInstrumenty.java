package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium07;

import pl.imiajd.stramko.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra=new ArrayList<>();
        orkiestra.add(new Flet("Aulos", LocalDate.of(2010,8,21)));
        orkiestra.add(new Fortepian("C. Bechstein", LocalDate.of(1985,5,1)));
        orkiestra.add(new Skrzypce("Yamaha", LocalDate.of(2009,12,30)));
        orkiestra.add(new Skrzypce("Stentor", LocalDate.of(2016,7,28)));
        orkiestra.add(new Flet("Mollenhauer", LocalDate.of(1996,3,12)));
        for(Instrument instrument : orkiestra)
        {
            System.out.println(instrument.dzwiek());
        }
        System.out.println();
        for (Instrument instrument : orkiestra)
        {
            System.out.println(instrument);
        }
    }
}
