package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium11;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class TestZadanie2 {
    public static void main(String[] args) {
        HashMap<String, String> oceny=new HashMap<>();
        TreeMap<String, String> ocenysort=new TreeMap<>();
        Scanner scnr=new Scanner(System.in);
        Scanner scnr2=new Scanner(System.in);
        Scanner scnr3=new Scanner(System.in);
        String wybor;
        String imie;
        String ocena;
        label:
        while(true)
        {
            System.out.println("Wybierz opcje z menu: dodaj, usun, zmien, wypisz, zakoncz");
            wybor=scnr.nextLine();
            switch (wybor) {
                case "dodaj":
                    System.out.println("Wprowadz imie:");
                    imie = scnr2.nextLine();
                    System.out.println("Wprowadz ocene:");
                    ocena = scnr3.nextLine();
                    oceny.put(imie, ocena);
                    break;
                case "usun":
                    System.out.println("Podaj imie ucznia do usuniecia:");
                    imie = scnr2.nextLine();
                    oceny.remove(imie);
                    ocenysort.remove(imie);
                    break;
                case "zmien":
                    System.out.println("Podaj imie ucznia do zmiany oceny");
                    imie = scnr2.nextLine();
                    System.out.println("Wprowadz nowa ocene:");
                    ocena = scnr3.nextLine();
                    oceny.replace(imie, ocena);
                    break;
                case "wypisz":
                    ocenysort.putAll(oceny);
                    for (Map.Entry<String, String> entry : ocenysort.entrySet()) {
                        System.out.println(entry.getKey() + ": " + entry.getValue());
                    }
                    break;
                case "zakoncz":
                    break label;
                default:
                    System.out.println("Operacja nie zawarta w menu lub nie mozliwa do wykoniania");
                    break;
            }
        }
    }
}
