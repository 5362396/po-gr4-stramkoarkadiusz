package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium11;

import java.util.PriorityQueue;
import java.util.Scanner;

public class TestZadanie1 {
    public static void main(String[] args) throws InterruptedException {
        PriorityQueue<Zadanie> kolejka=new PriorityQueue<>();
        Scanner scnr=new Scanner(System.in);
        Scanner scnr2=new Scanner(System.in);
        Scanner scnr3=new Scanner(System.in);
        int priorytet;
        String opis;
        String wybor="";
        while(true)
        {
            System.out.println("Wybierz opcje z menu: dodaj priorytet opis, nastepne, zakoncz");
            wybor=scnr.nextLine();
            if(wybor.equals("dodaj priorytet opis"))
            {
                System.out.println("Wprowadz priorytet:");
                priorytet=scnr2.nextInt();
                System.out.println("Wprowadz opis:");
                opis=scnr3.nextLine();
                kolejka.add(new Zadanie(priorytet, opis));
            }
            else if(wybor.equals("nastepne") && kolejka.size()!=0)
            {
                System.out.println(kolejka.poll().toString());
            }
            else if(wybor.equals("zakoncz"))
            {
                break;
            }
            else
            {
                System.out.println("Operacja nie zawarta w menu lub nie mozliwa do wykoniania");
            }
        }
    }
}
