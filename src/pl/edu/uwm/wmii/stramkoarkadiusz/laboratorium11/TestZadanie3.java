package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium11;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class TestZadanie3 {
    public static void main(String[] args) {
        ArrayList<Student> studenci=new ArrayList<>();
        TreeMap<Student, String> oceny=new TreeMap<>();
        Scanner scnr=new Scanner(System.in);
        String wybor;
        String imie;
        String nazwisko;
        int id;
        String ocena;
        label:
        while(true)
        {
            System.out.println("Wybierz opcje z menu: dodaj, usun, zmien, wypisz, zakoncz");
            wybor = scnr.next();
            switch (wybor) {
                case "dodaj": {
                    System.out.println("Wprowadz imie:");
                    imie = scnr.next();
                    System.out.println("Wprowadz nazwisko:");
                    nazwisko = scnr.next();
                    System.out.println("Wprowadz ocene:");
                    ocena = scnr.next();
                    Student st = new Student(imie, nazwisko);
                    studenci.add(st);
                    oceny.put(st, ocena);
                    break;
                }
                case "usun": {
                    System.out.println("Podaj id ucznia do usuniecia:");
                    id = scnr.nextInt();
                    Student st;
                    for (int i = 0; i < studenci.size(); i++) {
                        if (studenci.get(i).getStudent_id() == id) {
                            st = studenci.get(i);
                            studenci.remove(st);
                            oceny.remove(st);
                        }
                    }
                    break;
                }
                case "zmien": {
                    System.out.println("Podaj id ucznia do zmiany oceny");
                    id = scnr.nextInt();
                    System.out.println("Wprowadz nowa ocene:");
                    ocena = scnr.next();
                    Student st;
                    for (Student student : studenci) {
                        if (student.getStudent_id() == id) {
                            st = student;
                            oceny.replace(st, ocena);
                        }
                    }
                    break;
                }
                case "wypisz":
                    for (Map.Entry<Student, String> entry : oceny.entrySet()) {
                        System.out.println(entry.getKey().getNazwisko() + " " + entry.getKey().getImie() + " id: " + entry.getKey().getStudent_id() + " ocena: " + entry.getValue());
                    }
                    break;
                case "zakoncz":
                    break label;
                default:
                    System.out.println("Operacja nie zawarta w menu lub nie mozliwa do wykoniania");
                    break;
            }
        }
    }
}

