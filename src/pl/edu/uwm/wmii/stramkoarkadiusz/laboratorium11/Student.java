package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium11;

public class Student implements Comparable<Student> {
    private static int id = 0;
    private int student_id;
    private String imie;
    private String nazwisko;

    public Student(String imie, String nazwisko)
    {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.student_id = id;
        id++;
    }

    public int getStudent_id()
    {
        return student_id;
    }

    public String getImie()
    {
        return imie;
    }

    public String getNazwisko()
    {
        return nazwisko;
    }

    @Override
    public int compareTo(Student st)
    {
        if(nazwisko.compareTo(st.nazwisko) != 0)
            return nazwisko.compareTo(st.nazwisko);
        else if(imie.compareTo(st.imie) != 0)
            return imie.compareTo(st.imie);
        else if(student_id > st.student_id)
            return 1;
        else if(student_id < st.student_id)
            return -1;
        return 0;
    }
}
