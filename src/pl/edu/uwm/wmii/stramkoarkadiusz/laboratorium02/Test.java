package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium02;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scnr=new Scanner(System.in);
        int n;
        System.out.println("Podaj liczbe calkowita w zakresie 1<=n<=100 jako ilosc elementow tablicy w zadaniu 1");
        n=scnr.nextInt();
        System.out.println("Zadanie 1a:");
        Zadanie1.a(n);
        System.out.println("Zadanie 1b:");
        Zadanie1.b(n);
        System.out.println("Zadanie 1c:");
        Zadanie1.c(n);
        System.out.println("Zadanie 1d:");
        Zadanie1.d(n);
        System.out.println("Zadanie 1e:");
        Zadanie1.e(n);
        System.out.println("Zadanie 1f:");
        Zadanie1.f(n);
        System.out.println("Zadanie 1g:");
        Zadanie1.g(n);
        System.out.println("Podaj liczbe calkowita w zakresie 1<=n<=100 jako ilosc elementow tablicy w zadaniu 2");
        n=scnr.nextInt();
        int tab[]=new int[n];
        Zadanie2.generuj(tab, n, -999, 999);
        System.out.println("Zadanie 2a:");
        System.out.println("Ilosc liczb nieparzystych w tablicy: "+Zadanie2.ileNieparzystych(tab));
        System.out.println("Ilosc liczb parzystych w tablicy: "+Zadanie2.ileParzystych(tab));
        System.out.println("Zadanie 2b:");
        System.out.println("Ilosc liczb dodatnich w tablicy: "+Zadanie2.ileDodatnich(tab));
        System.out.println("Ilosc liczb ujemnych w tablicy: "+Zadanie2.ileUjemnych(tab));
        System.out.println("Ilosc zer w tablicy: "+Zadanie2.ileZerowych(tab));
        System.out.println("Zadanie 2c:");
        System.out.println("Ilosc elementow maksymalnych w tablicy: "+Zadanie2.ileMaksymalnych(tab));
        System.out.println("Zadanie 2d:");
        System.out.println("Suma dodatnich elementów tablicy: "+Zadanie2.sumaDodatnich(tab));
        System.out.println("Suma ujemnych elementów tablicy: "+Zadanie2.sumaUjemnych(tab));
        System.out.println("Zadanie 2e:");
        System.out.println("Dlugosc najdluzszego fragmentu tablicy z dodatnimi elementami: "+Zadanie2.dlugoscMaksymalnegoCiaguDodatnich(tab));
        System.out.println("Zadanie 2f:");
        Zadanie2.signum(tab);
        System.out.println("Zadanie 2g:");
        System.out.println("Wprowadz kolejno indeksy dwoch elementow tablicy w zakresie 1<=indeksy<=n, pierwszy wpisywany indeks musi byc mniejszy od drugiego");
        System.out.println("Wprowadz pierwszy indeks:");
        int lewy = scnr.nextInt()-1;
        System.out.println("Wprowadz drugi indeks:");
        int prawy = scnr.nextInt()-1;
        Zadanie2.odwrocFragment(tab, lewy, prawy);
        System.out.println("Zadanie 3:");
        Zadanie3.macierze();
    }
}
