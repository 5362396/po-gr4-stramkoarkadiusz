package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium02;

import java.util.Random;

public class Zadanie2 {
    public static void generuj(int tab[], int n, int min, int max)
    {
        Random random=new Random();
        for(int i=0; i<n; i++)
        {
            tab[i]=random.nextInt(max-min)+min;
        }
    }
    public static int ileNieparzystych(int tab[])
    {
        int nieparzyste=0;
        for (int j : tab)
        {
            if (j%2!=0)
            {
                nieparzyste++;
            }
        }
        return nieparzyste;
    }
    public static int ileParzystych(int tab[])
    {
        int parzyste=0;
        for (int j : tab)
        {
            if (j%2==0)
            {
                parzyste++;
            }
        }
        return parzyste;
    }
    public static int ileDodatnich(int tab[])
    {
        int dodatnie=0;
        for (int j : tab)
        {
            if (j>0)
            {
                dodatnie++;
            }
        }
        return dodatnie;
    }
    public static int ileUjemnych(int tab[])
    {
        int ujemne=0;
        for (int j : tab)
        {
            if (j<0)
            {
                ujemne++;
            }
        }
        return ujemne;
    }
    public static int ileZerowych(int tab[])
    {
        int zera=0;
        for (int j : tab)
        {
            if (j==0)
            {
                zera++;
            }
        }
        return zera;
    }
    public static int ileMaksymalnych(int tab[])
    {
        int max=-1000;
        int powtorka=1;
        for (int j : tab)
        {
            if (j>max)
            {
                powtorka=1;
                max=j;
            }
            if (j==max)
            {
                powtorka++;
            }
        }
        return powtorka;
    }
    public static int sumaDodatnich(int tab[])
    {
        int sumaplus=0;
        for (int j : tab)
        {
            if (j>0)
            {
                sumaplus=sumaplus+j;
            }
        }
        return sumaplus;
    }
    public static int sumaUjemnych(int tab[])
    {
        int sumaminus=0;
        for (int j : tab)
        {
            if (j<0)
            {
                sumaminus=sumaminus+j;
            }
        }
        return sumaminus;
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[])
    {
        int dlugosc=0;
        int dlugoscmax=0;
        for (int j : tab)
        {
            if (j>0)
            {
                dlugosc++;
            }
            else
            {
                if (dlugosc>dlugoscmax)
                {
                    dlugoscmax=dlugosc;
                }
                dlugosc=0;
            }
        }
        return dlugoscmax;
    }
    public static void signum(int tab[])
    {
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i]>0)
            {
                tab[i]=1;
            }
            else if(tab[i]<0)
            {
                tab[i]=-1;
            }
        }
        for (int j : tab) {
            System.out.println(j);
        }
    }
    public static void odwrocFragment(int tab[], int lewy, int prawy)
    {
        int zm;
        int x=0;
        System.out.println("Tablica pierwotna:");
        for (int j : tab)
        {
            System.out.println(j);
        }
        System.out.println("Tablica po zmianach:");
        for(int i=lewy; i<prawy-1; i++)
        {
            zm=tab[i];
            tab[i]=tab[prawy-x];
            tab[prawy-x]=zm;
            x++;
        }
        for (int j : tab) {
            System.out.println(j);
        }
    }
}
