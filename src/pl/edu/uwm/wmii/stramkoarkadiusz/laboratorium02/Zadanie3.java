package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium02;

import java.util.Scanner;
import java.util.Random;

public class Zadanie3 {
    public static void macierze()
    {
        Scanner scnr=new Scanner(System.in);
        int m=scnr.nextInt();
        int n=scnr.nextInt();
        int k=scnr.nextInt();
        Random random=new Random();

        int macierz_a[][]=new int[m][n];
        int macierz_b[][]=new int[n][k];
        int macierz_c[][]=new int[macierz_a.length][macierz_b[0].length];

        for(int i=0; i<m; i++)
        {
            for(int j=0; j<n; j++)
            {
                macierz_a[i][j]=random.nextInt(10)+1;
            }
        }

        for(int i=0; i<macierz_a.length; i++)
        {
            for(int j=0; j<macierz_a[i].length; j++)
            {
                System.out.print(macierz_a[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println();

        for(int i=0; i<n; i++)
        {
            for(int j=0; j<k; j++)
            {
                macierz_b[i][j]=random.nextInt(10)+1;
            }
        }

        for(int i=0; i<macierz_b.length; i++)
        {
            for(int j=0; j<macierz_b[i].length; j++)
            {
                System.out.print(macierz_b[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println();

        if(macierz_a[0].length==macierz_b.length)
        {
            for(int i=0; i<macierz_c.length; i++)
            {
                for(int j=0; j<macierz_c[i].length; j++)
                {
                    for (int x=0; x<macierz_a[0].length; x++) {
                        macierz_c[i][j]+=macierz_a[i][x]*macierz_b[x][j];
                    }
                }
            }
        }

        for(int i=0; i<macierz_c.length; i++)
        {
            for(int j=0; j<macierz_c[i].length; j++)
            {
                System.out.print(macierz_c[i][j]+" ");
            }
            System.out.println();
        }
    }
}
