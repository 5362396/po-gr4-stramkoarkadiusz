package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1 {
    public static void a(int n)
    {
        Random random=new Random();
        int tab[]=new int[n];
        int parzyste=0;
        for(int i=0; i<n; i++)
        {
            tab[i]=random.nextInt(1998)-999;
            if(tab[i]%2==0)
            {
                parzyste++;
            }
        }
        int nieparzyste=n-parzyste;
        System.out.println("Ilosc liczb nieparzystych w tablicy: "+nieparzyste);
        System.out.println("Ilosc liczb parzystych w tablicy: "+parzyste);
    }
    public static void b(int n)
    {
        Random random=new Random();
        int tab[]=new int[n];
        int dodatnie=0;
        int ujemne=0;
        int zera=0;
        for(int i=0; i<n; i++)
        {
            tab[i]=random.nextInt(1998)-999;
            if(tab[i]>0)
            {
                dodatnie+=1;
            }
            else if(tab[i]<0)
            {
                ujemne+=1;
            }
            else
            {
                zera+=1;
            }
        }
        System.out.println("Ilosc liczb dodatnich w tablicy: "+dodatnie);
        System.out.println("Ilosc liczb ujemnych w tablicy: "+ujemne);
        System.out.println("Ilosc zer w tablicy: "+zera);
    }
    public static void c(int n)
    {
        Random random=new Random();
        int tab[]=new int[n];
        int max=-1000;
        int powtorka=1;
        for(int i=0; i<n; i++)
        {
            tab[i]=random.nextInt(1998)-999;
            if(tab[i]>max)
            {
                powtorka=1;
                max=tab[i];
            }
            if(tab[i]==max) {
                powtorka++;
            }
        }
        System.out.println("Element najwiekszy w tablicy: "+max);
        System.out.println("Ilosc wystapien: "+powtorka);
    }
    public static void d(int n)
    {
        Random random=new Random();
        int tab[]=new int[n];
        int sumaplus=0;
        int sumaminus=0;
        for(int i=0; i<n; i++)
        {
            tab[i]=random.nextInt(1998)-999;
            if(tab[i]>0)
            {
                sumaplus=sumaplus+tab[i];
            }
            else if(tab[i]<0)
            {
                sumaminus=sumaminus+tab[i];
            }

        }
        System.out.println("Suma ujemnych elementów tablicy: "+sumaminus);
        System.out.println("Suma dodatnich elementów tablicy: "+sumaplus);
    }
    public static void e(int n)
    {
        Random random=new Random();
        int tab[]=new int[n];
        int dlugosc=0;
        int dlugoscmax=0;
        for(int i=0; i<n; i++)
        {
            tab[i]=random.nextInt(1998)-999;
        }
        for(int i=0; i<n; i++)
        {
            if(tab[i]>0)
            {
                dlugosc++;
            }
            else
            {
                if(dlugosc>dlugoscmax)
                {
                    dlugoscmax=dlugosc;
                }
                dlugosc=0;
            }
        }
        System.out.println("Dlugosc najdluzszego fragmentu tablicy z dodatnimi elementami: "+dlugoscmax);
    }
    public static void f(int n)
    {
        Random random=new Random();
        int tab[]=new int[n];
        for(int i=0; i<n; i++)
        {
            tab[i]=random.nextInt(1998)-999;
            if(tab[i]>0)
            {
                tab[i]=1;
            }
            else if(tab[i]<0)
            {
                tab[i]=-1;
            }
        }
        for(int i=0; i<n; i++)
        {
            System.out.println(tab[i]);
        }
    }
    public static void g(int n)
    {
        Random random=new Random();
        Scanner scnr=new Scanner(System.in);
        int tab[]=new int[n];
        System.out.println("Wprowadz kolejno indeksy dwoch elementow tablicy w zakresie 1<=indeksy<=n, pierwszy wpisywany indeks musi byc mniejszy od drugiego");
        System.out.println("Wprowadz pierwszy indeks:");
        int lewy=scnr.nextInt()-1;
        System.out.println("Wprowadz drugi indeks:");
        int prawy=scnr.nextInt()-1;
        int zm;
        int x=0;
        System.out.println("Tablica pierwotna:");
        for(int i=0; i<n; i++)
        {
            tab[i]=random.nextInt(1998)-999;
            System.out.println(tab[i]);
        }
        System.out.println("Tablica po zmianach:");
        for(int i=lewy; i<prawy-1; i++)
        {
            zm=tab[i];
            tab[i]=tab[prawy-x];
            tab[prawy-x]=zm;
            x++;
        }
        for(int i=0; i<n; i++)
        {
            System.out.println(tab[i]);
        }
    }
}
