package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium05;

public class IntegerSet {
    private boolean[] tablica;
    public IntegerSet()
    {
        this.tablica=new boolean[100];
    }
    public static IntegerSet union(IntegerSet a, IntegerSet b)
    {
        IntegerSet suma=new IntegerSet();
        for(int i=0; i<100; i++)
        {
            if(a.tablica[i] || b.tablica[i])
            {
                suma.tablica[i]=true;
            }
        }
        return suma;
    }
    public static IntegerSet intersection(IntegerSet a, IntegerSet b)
    {
        IntegerSet iloczyn=new IntegerSet();
        for(int i=0; i<100; i++)
        {
            if(a.tablica[i] && b.tablica[i])
            {
                iloczyn.tablica[i]=true;
            }
        }
        return iloczyn;
    }
    public void insertElement(int n)
    {
        this.tablica[n-1]=true;
    }
    public void deleteElement(int n)
    {
        this.tablica[n-1]=false;
    }
    @Override
    public String toString()
    {
        StringBuffer liczby=new StringBuffer();
        for(int i=0; i<100; i++)
        {
            if(this.tablica[i])
            {
                liczby.append(i+1);
                liczby.append(" ");
            }
        }
        return liczby.toString();
    }
    public boolean equals(IntegerSet a)
    {
        return this.toString().equals(a.toString());
    }
}
