package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium05;

public class Test {
    public static void main(String[] args) {
        System.out.println("Zadanie 1:");
        RachunekBankowy saver1=new RachunekBankowy(2000.0);
        RachunekBankowy saver2=new RachunekBankowy(3000.0);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());
        System.out.println("Zadanie 2:");
        IntegerSet x=new IntegerSet();
        IntegerSet y=new IntegerSet();
        x.insertElement(2);
        x.insertElement(1);
        x.insertElement(3);
        x.insertElement(7);
        y.insertElement(1);
        y.insertElement(2);
        y.insertElement(3);
        y.insertElement(91);
        System.out.println(x);
        System.out.println(y);
        System.out.println(IntegerSet.union(x,y));
        System.out.println(IntegerSet.intersection(x,y));
        System.out.println(x.equals(y));
        y.deleteElement(91);
        y.insertElement(7);
        System.out.println(y);
        System.out.println(x.equals(y));
        System.out.println("Zadanie 3:");
        Pracownik[] personel = new Pracownik[3];

        // wypełnij tablicę danymi pracowników
        personel[0] = new Pracownik("Karol Cracker", 75000, 2001, 12, 15);
        personel[1] = new Pracownik("Henryk Hacker", 50000, 2003, 10, 1);
        personel[2] = new Pracownik("Antoni Tester", 40000, 2005, 3, 15);

        // zwiększ pobory każdego pracownika o 20%
        for (Pracownik e : personel) {
            e.zwiekszPobory(20);
        }

        // wypisz informacje o każdym pracowniku
        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tid = " + e.id());
            System.out.print("\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

        int n = Pracownik.getNextId(); // wywołanie metody statycznej
        System.out.println("Następny dostępny id = " + n);
    }
}
