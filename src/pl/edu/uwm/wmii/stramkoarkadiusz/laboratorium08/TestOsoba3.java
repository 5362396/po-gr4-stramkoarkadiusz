package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium08;

import pl.imiajd.stramko.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba3 {
    public static void main(String[] args) {
        ArrayList<Osoba3> grupa = new ArrayList<>();
        grupa.add(0, new Osoba3("Kowalski", LocalDate.of(1989, 12, 16)));
        grupa.add(1, new Osoba3("Nowak", LocalDate.of(1967, 4, 22)));
        grupa.add(2, new Osoba3("Kowalski", LocalDate.of(2006, 8, 3)));
        grupa.add(3, new Osoba3("Kubiak", LocalDate.of(1967, 4, 22)));
        grupa.add(4, new Osoba3("Stramko", LocalDate.of(2000, 5, 11)));
        for(int i=0; i<5; i++)
        {
            System.out.println(grupa.get(i).toString());
        }
        Collections.sort(grupa);
        System.out.println("Posortowane:");
        for(int i=0; i<5; i++)
        {
            System.out.println(grupa.get(i).toString());
        }
    }
}
