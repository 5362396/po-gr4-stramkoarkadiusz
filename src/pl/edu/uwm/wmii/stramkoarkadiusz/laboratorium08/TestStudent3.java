package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium08;

import pl.imiajd.stramko.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent3 {
    public static void main(String[] args) {
        ArrayList<Student3> grupa = new ArrayList<>();
        grupa.add(0, new Student3("Kowalski", LocalDate.of(1989, 12, 16), 4.76));
        grupa.add(1, new Student3("Nowak", LocalDate.of(1967, 4, 22), 3.78));
        grupa.add(2, new Student3("Kowalski", LocalDate.of(2006, 8, 3), 4.56));
        grupa.add(3, new Student3("Kubiak", LocalDate.of(1967, 4, 22), 4.23));
        grupa.add(4, new Student3("Stramko", LocalDate.of(2000, 5, 11), 4.89));
        for(int i=0; i<5; i++)
        {
            System.out.println(grupa.get(i).toString());
        }
        Collections.sort(grupa);
        System.out.println("Posortowane:");
        for(int i=0; i<5; i++)
        {
            System.out.println(grupa.get(i).toString());
        }
    }
}
