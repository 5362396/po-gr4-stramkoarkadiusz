package pl.edu.uwm.wmii.stramkoarkadiusz.laboratorium08;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) {
        ArrayList<String> odczyt=new ArrayList<>();
        try
        {
            File plik=new File("src/pl/edu/uwm/wmii/stramkoarkadiusz/laboratorium08/pliktekstowy.txt");
            Scanner scnr =new Scanner(plik);
            while(scnr.hasNextLine())
            {
                odczyt.add(scnr.nextLine());
            }
            scnr.close();
        }
        catch (FileNotFoundException e)
        {
            System.out.println("Nie ma takiego pliku");
            e.printStackTrace();
        }
        System.out.println(odczyt);
        Collections.sort(odczyt);
        System.out.println(odczyt);
    }
}
